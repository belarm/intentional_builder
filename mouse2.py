#!/usr/bin/env python3
"""Place a rectangle with the mouse."""
import os
import numpy as np
# os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame
import sys
from pygame.locals import *
RED = (255, 0, 0, 255)
GREEN = (0, 255, 0, 255)
GRAY = (127, 127, 127, 255)

pygame.init()
screen = pygame.display.set_mode((640, 240), pygame.RESIZABLE)
polys = []
poly = []
drawing = False
running = True
clock = pygame.time.Clock()

# Generalize and also this is a really bad idea.

corn_surface = pygame.transform.scale(pygame.image.load("./icons/ffffff/000000/1x1/delapouite/corn.png"), (32,32))
highgrass_surface = pygame.transform.scale(pygame.image.load("./icons/ffffff/000000/1x1/delapouite/high-grass.png"), (32,32))


def bbox(perimeter):
    return np.array([np.min(perimeter, axis=0), np.max(perimeter, axis=0)])

class Item(object):
    def __init__(self, type, perimeter, surface, layer=1):
        self.type = type
        self.perimeter = perimeter
        self.offset = offset
        self.surface = surface
        self.layer = layer

    def __str__(self):
        return f"Item of type {self.type} with perimeter {self.perimeter}"

    def __repr__(self):
        return f"{self.__class__.__name__}({self.type.__repr__()},{self.perimeter},{self.layer})"

#Actually, let's start that class:
class World(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.objects = {}
        self.surface = pygame.Surface((x, y))
        for i in range(0,x,10):
            for j in range(0,y,10):
                if (i/10 + j/10) % 2 == 0:
                    color = (0,0,0)
                else:
                    color = (255,255,255)
                self.surface.fill(color, Rect(i, j, 10, 10))

    def add_item(self, item):
        if item.layer not in self.objects:
            self.objects[item.layer] = []
        self.objects[item.layer].append(item)

        box = bbox(item.perimeter)
        poly_size = box[1] - box[0]
        width, height = poly_size
        temp_surface = pygame.Surface(poly_size, SRCALPHA)
        temp_surface.fill((0,0,0,0))
        pygame.draw.polygon(temp_surface, GREEN, item.perimeter - box[0])
        fill_surface = pygame.Surface(poly_size, SRCALPHA)
        for i in range(0, width, 32):
            for j in range(0, height, 32):
                fill_surface.blit(item.surface, (i,j))
        fill_alpha = pygame.surfarray.pixels_alpha(fill_surface)
        fill_alpha *= (pygame.surfarray.array_alpha(temp_surface) > 0).astype(np.uint8)
        del fill_alpha
        self.surface.blit(fill_surface, box[0])


MOVEMENTS = {
    pygame.K_DOWN : np.array([0,-1]),
    pygame.K_UP : np.array([0,1]),
    pygame.K_LEFT : np.array([1,0]),
    pygame.K_RIGHT : np.array([-1,0])
}
movement = np.array([0,0])
offset = np.array([0,0])
world = World(1000,1000)
world.add_item(Item("topsoil", np.array([[0,0], [world.x, 0], [world.x, world.y], [0, world.y]]), highgrass_surface))
while running:
    for event in pygame.event.get():
        if event.type == VIDEORESIZE:
            pygame.display.update()
        if event.type == QUIT:
            running = False
        elif event.type == MOUSEBUTTONDOWN:
            if event.button == 1:
                poly.append(event.pos)
                end = event.pos
                drawing = True
            elif event.button == 3:
                if poly:
                    item = Item("farm", np.array(poly) - offset, corn_surface)
                    world.add_item(item)
                    polys.append(poly)
                    poly = []
                    drawing = False

        elif event.type == MOUSEMOTION and drawing:
            end = event.pos
        # Super clever way to allow scrolling. Defintely unique to me /s
        elif event.type == pygame.KEYDOWN:
            if event.key in MOVEMENTS:
                newmovement = MOVEMENTS[event.key]
                movement += newmovement
        elif event.type == pygame.KEYUP:
            if event.key in MOVEMENTS:
                newmovement = MOVEMENTS[event.key]
                movement -= newmovement

    offset = (offset[0] + movement[0], offset[1] + movement[1])
    screen.blit(world.surface, offset)

    if drawing:
        pygame.draw.polygon(screen, RED, poly + [poly[-1]] * 2)

    pygame.display.flip()
    clock.tick(50)
pygame.quit()
